my()
{
    #mysql --user='alice' --password='MyStrongPass.' --database='avm' -h '127.0.0.1' --local-infile=1 --execute="$1"
    mysql --user='admin' --password='s4erjd9edid9odb' --database='avm' -h 'liqquid-r5-xlarge.cokbmcletl0d.eu-west-2.rds.amazonaws.com' --local-infile=1 --execute="$1"
}



my "DROP TABLE IF EXISTS epc_scrape;"


my "CREATE TABLE \`epc_scrape\` (
  \`postcode\` varchar(10) DEFAULT NULL,
   \`address\`  varchar(255) DEFAULT NULL,
  \`certificate\` varchar(255) DEFAULT NULL,
  \`TOTAL_FLOOR_AREA\` double DEFAULT NULL,
  \`INSPECTION_DATE\` varchar(10) DEFAULT NULL,
  \`LODGEMENT_DATE\` varchar(10) DEFAULT NULL
 ) ENGINE=InnoDB DEFAULT CHARSET=latin1;"


# uploading data from csv
my "LOAD DATA LOCAL INFILE './EPC_missing_M2_2021_11_14.csv' INTO TABLE epc_scrape
FIELDS OPTIONALLY ENCLOSED BY '\"'
TERMINATED BY ','
LINES TERMINATED BY '\n'
IGNORE 1 ROWS
(\`postcode\`, \`address\`, \`certificate\`, \`TOTAL_FLOOR_AREA\`, \`INSPECTION_DATE\`, \`LODGEMENT_DATE\`);"

#
# POSTCODE, Address, Certificate, TOTAL_FLOOR_AREA, INSPECTION_DATE, LODGEMENT_DATE
# AL1 4JW, "1, St. Pauls Place, Hatfield Road, ST. ALBANS, AL1 4JW", 2788-4039-6278-5151-7934,51, 28/08/2019, 29/08/2019


#'table format:'
#Datetext       , RegionName, AreaCode, AveragePrice, Index, IndexSA, 1m%Change, 12m%Change, AveragePriceSA, SalesVolume, DetachedPrice, DetachedIndex, Detached1m%Change, Detached12m%Change, SemiDetachedPrice, SemiDetachedIndex, SemiDetached1m%Change, SemiDetached12m%Change, TerracedPrice, TerracedIndex, Terraced1m%Change, Terraced12m%Change, FlatPrice, FlatIndex, Flat1m%Change, Flat12m%Change, CashPrice, CashIndex, Cash1m%Change, Cash12m%Change, CashSalesVolume, MortgagePrice, MortgageIndex, Mortgage1m%Change, Mortgage12m%Change, MortgageSalesVolume, FTBPrice, FTBIndex, FTB1m%Change, FTB12m%Change, FOOPrice, FOOIndex, FOO1m%Change, FOO12m%Change, NewPrice, NewIndex, New1m%Change, New12m%Change, NewSalesVolume, OldPrice, OldIndex, Old1m%Change, Old12m%Change, OldSalesVolume, Date
#Date           , RegionName, AreaCode, AveragePrice, Index, IndexSA, 1m%Change, 12m%Change, AveragePriceSA, SalesVolume, DetachedPrice, DetachedIndex, Detached1m%Change, Detached12m%Change, SemiDetachedPrice, SemiDetachedIndex, SemiDetached1m%Change, SemiDetached12m%Change, TerracedPrice, TerracedIndex, Terraced1m%Change, Terraced12m%Change, FlatPrice, FlatIndex, Flat1m%Change, Flat12m%Change, CashPrice, CashIndex, Cash1m%Change, Cash12m%Change, CashSalesVolume, MortgagePrice, MortgageIndex, Mortgage1m%Change, Mortgage12m%Change, MortgageSalesVolume, FTBPrice, FTBIndex, FTB1m%Change, FTB12m%Change, FOOPrice, FOOIndex, FOO1m%Change, FOO12m%Change, NewPrice, NewIndex, New1m%Change, New12m%Change, NewSalesVolume, OldPrice, OldIndex, Old1m%Change, Old12m%Change, OldSalesVolume

echo '\n csv uploaded \n'
